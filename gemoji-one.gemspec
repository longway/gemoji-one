Gem::Specification.new do |s|
  s.name    = "gemoji-one"
  s.version = "1.0.0"
  s.summary = "Emoji conversion and image assets"
  s.description = "Image assets and character information for emoji with emoji-one."

  s.required_ruby_version = '> 1.9'

  s.authors  = ["Kesha Antonov"]
  s.email    = "innokenty.longway@gmail.com"
  s.homepage = ""
  s.licenses = ["MIT"]

  s.files = Dir[
    "README.md",
    "images/**/*.svg",
    "db/emoji.json",
    "lib/**/*.rb",
    "lib/tasks/*.rake"
  ]
end
